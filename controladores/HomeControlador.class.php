<?php


class HomeControlador
{
    public static function home()
    {
        return generarHtml("home/principal", []);
    }

    public static function login($request)
    {
        if ($request['post']['email'] === "test@gmail.com" && $request['post']['password'] === "1234") {
            $_SESSION['logueado'] = true;
            header('Location: /productos');
            return;
        }
        header('Location: /');
    }
}
