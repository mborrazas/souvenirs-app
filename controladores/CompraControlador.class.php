<?php


class CompraControlador
{
    public static function procesarComprar($request)
    {
        $producto = new ProductoModelo();
        $producto->setId((int)  $request['post']['producto']);
        $producto = $producto->getById();
        if ($producto instanceof ProductoModelo) {
            if ($producto->getStock() < ((int)$request['post']['cantidad'])) {
                echo 'No hay suficiente Stock';
                return;
            }
            $compra = new CompraModelo();
            $compra->setProducto((int) $request['post']['producto']);
            $compra->setCantidad((int) $request['post']['cantidad']);
            $compra->setFechaDeCompra(new DateTime());
            $compra->guardar();
            $producto->setStock($producto->getStock() - ((int) $request['post']['cantidad']));
            $producto->actualizar();
            header('Location: /compras/listar');
            return;
        }
        echo 'El producto no existe';
        return;
    }

    public static function comprar()
    {
        $producto = new ProductoModelo();
        $productos = $producto->listar();
        return generarHtml("compras/comprar", ["productos" => $productos]);
    }

    public static function listar()
    {
        $compra = new CompraModelo();
        $compras = $compra->listar();
        return generarHtml("compras/listar", ["compras" => $compras]);
    }
}
