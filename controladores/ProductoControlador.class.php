<?php


class ProductoControlador
{
    public static function principal()
    {
        $p = new ProductoModelo();
        $productos = $p->listar();
        return generarHtml("producto/principal", ["productos" => $productos]);
    }

    public static function editar($request)
    {
        $p = new ProductoModelo();
        $p->setId($request['get']['id']);
        $producto = $p->getById();
        return generarHtml("producto/editar", ["producto" => $producto]);
    }

    public static function procesarEditar($request)
    {
        try {
            $p = new ProductoModelo();
            $p->setNombre($request['post']['nombre']);
            $p->setId((int)$request['post']['id']);
            $p->setPrecio((float) $request['post']['precio']);
            $p->setStock((int) $request['post']['stock']);
            $p->setDescripcion($request['post']['descripcion']);
            $p->actualizar();
            header('Location: /productos');
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
    }

    public static function eliminar($request)
    {
        $p = new ProductoModelo();
        $p->setId($request['get']['id']);
        $p->eliminar();
        header('Location: /productos');
    }

    public static function crear()
    {
        return generarHtml("producto/crear", []);
    }

    public static function procesarCrear($request)
    {

        try {
            $p = new ProductoModelo();
            $p->setNombre($request['post']['nombre']);
            $p->setPrecio((float) $request['post']['precio']);
            $p->setFechaAlta(new DateTime());
            $p->setStock((int) $request['post']['stock']);
            $p->setDescripcion($request['post']['descripcion']);
            $p->guardar();
            header('Location: /productos');
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
    }
}
