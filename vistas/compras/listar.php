<?php

$compras = $parametros['compras'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Compras</title>
</head>

<body>
    <a href="/compras/comprar">Comprar</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Cantidad</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($compras as $compra) { ?>
                <tr>
                    <td><?php echo $compra->getId(); ?></td>
                    <td><?php echo $compra->getCantidad(); ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>

</html>