<?php

$productos = $parametros['productos'];


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Productos</title>
</head>

<body>
    <a href="/productos/crear">Crear</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Stock</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>

            <?php foreach ($productos as $producto) { ?>
                <tr>
                    <td><?php echo $producto->getId(); ?></td>
                    <td><?php echo $producto->getNombre(); ?></td>
                    <td><?php echo $producto->getStock(); ?></td>
                    <td><a href="/productos/editar?id=<?php echo $producto->getId(); ?>">Editar</a></td>
                </tr>

            <?php } ?>
        </tbody>
    </table>
</body>

</html>