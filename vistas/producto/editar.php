<?php

$producto = $parametros['producto'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Producto</title>
</head>

<body>
    <form action="/productos/procesarEditar" method="POST">
        <input type="text" hidden name="id" placeholder="id" value="<?php echo $producto->getId(); ?>" />
        <label for="nombre">Nombre:</label>
        <input type="text" name="nombre" placeholder="Nombre" value="<?php echo $producto->getNombre(); ?>" />
        <label for="descripcion">Descripcion:</label>
        <input type="text" name="descripcion" placeholder="Descripcion" value="<?php echo $producto->getDescripcion(); ?>" />
        <label for="stock">Stock:</label>
        <input type="number" name="stock" placeholder="Stock" value="<?php echo $producto->getStock(); ?>" />
        <label for="precio">Precio:</label>
        <input type="number" name="precio" placeholder="Precio" value="<?php echo $producto->getPrecio(); ?>" />
        <input type="submit" value="Guardar">
    </form>
    <a href="/productos/eliminar?id=<?php echo $producto->getId(); ?>">Eliminar</a>
</body>

</html>