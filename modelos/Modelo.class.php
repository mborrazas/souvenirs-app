<?php

require $_SERVER["DOCUMENT_ROOT"]."/../config.php";
class Modelo
{
    protected $IpDB;
    protected $UsuarioDB;
    protected $PasswordDB;
    protected $NombreDB;
    protected $conexion;

    public function __construct()
    {
        $this->inicializarConexion();
        try{
            $this->conexion = new mysqli(
                $this->IpDB,
                $this->UsuarioDB,
                $this->PasswordDB,
                $this->NombreDB
            );
        }catch(Exception $e){
            var_dump($e->getMessage());die;
        }
       
    }

    protected function inicializarConexion()
    {
             
        $this->IpDB = IP_DB;
        $this->UsuarioDB = USUARIO_DB;
        $this->PasswordDB = PASSWORD_DB;
        $this->NombreDB = NOMBRE_DB;
    }
}
