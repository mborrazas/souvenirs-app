<?php
include_once('Modelo.class.php');


class ProductoModelo extends Modelo
{
    private $nombre;
    private $id;
    private $stock;
    private $precio;
    private $descripcion;
    private $fechaAlta;

    public function setNombre(string $nombre){
        $this->nombre = $nombre;
    }

    public function setId(int $id){
        $this->id = $id;
    }

    public function setStock(int $stock){
        $this->stock = $stock;
    }

    public function setPrecio(float $precio){
        $this->precio = $precio;
    }

    public function setDescripcion(string $descripcion){
        $this->descripcion = $descripcion;
    }

    public function setFechaAlta(DateTime $fechaAlta){
        $this->fechaAlta = $fechaAlta;
    }

    public function getNombre(): string{
        return $this->nombre;
    }

    public function getId(): int{
        return $this->id;
    }

    public function getStock(): int{
        return $this->stock;
    }

    public function getPrecio(): int{
        return $this->precio;
    }

    public function getDescripcion(): string{
        return $this->descripcion;
    }

    public function getFechaAlta(): DateTime{
        return $this->fechaAlta;
    }

    public function guardar()
    {
        $sql = "INSERT INTO producto(nombre,stock,precio,descripcion,fechaAlta) VALUES(
                '{$this->getNombre()}',
                '{$this->getStock()}',
                '{$this->getPrecio()}',
                '{$this->getDescripcion()}',
                '{$this->getFechaAlta()->format('Y-m-d H:i:s')}');";
        $this->conexion->query($sql);
        $this->setId($this->conexion->insert_id);
        return true;
    }

    public function eliminar()
    {
        $sql = "DELETE FROM producto where id = {$this->getId()}";
        $this->conexion->query($sql);
        return true;
    }

    

    public function actualizar()
    {
        $sql = "UPDATE producto set 
        nombre = '{$this->getNombre()}',
        stock = '{$this->getStock()}',
        precio = '{$this->getPrecio()}',
        descripcion = '{$this->getDescripcion()}'
        where id = {$this->getId()}";
        $this->conexion->query($sql);
        return true;
    }

    public function listar()
    {
        $sql = "SELECT * FROM producto";
        $resultado = $this->conexion->query($sql);
        return $this->generarArrayProductos($resultado);
    }

    public function getById(){
        $sql = "SELECT * FROM producto where id = '{$this->getId()}'";
        $resultado = $this->conexion->query($sql);
        return $this->generarProducto($resultado);
    }

    private function generarArrayProductos($productos)
    { 
        $coleccionProductos = [];
        while ($row = $productos->fetch_assoc()) {
            $p = new self();
            $p->setId($row['id']);
            $p->setNombre($row['nombre']);
            $p->setStock($row['stock']);
            $p->setPrecio($row['precio']);
            $p->setFechaAlta(new Datetime($row['fechaAlta']));
            $coleccionProductos[] = $p;
        }
        return $coleccionProductos;
    }

    private function generarProducto($producto)
    { 
        while ($row = $producto->fetch_assoc()) {
            $p = new self();
            $p->setId($row['id']);
            $p->setNombre($row['nombre']);
            $p->setStock($row['stock']);
            $p->setPrecio($row['precio']);
            $p->setDescripcion($row['descripcion']);
            $p->setFechaAlta(new Datetime($row['fechaAlta']));
            return $p;
        }
        return false;
    }

}
