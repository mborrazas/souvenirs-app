<?php
require $_SERVER["DOCUMENT_ROOT"] . "/../routes/routes.class.php";
Routes::Add("/", "get", "HomeControlador::home");
Routes::Add("/login", "post", "HomeControlador::login");
Routes::Add("/productos", "get", "ProductoControlador::principal", "AuthMiddleware::authorized");
Routes::Add("/productos/editar", "get", "ProductoControlador::editar", "AuthMiddleware::authorized");
Routes::Add("/productos/procesarEditar", "post", "ProductoControlador::procesarEditar", "AuthMiddleware::authorized");
Routes::Add("/productos/eliminar", "get", "ProductoControlador::eliminar", "AuthMiddleware::authorized");
Routes::Add("/productos/crear", "get", "ProductoControlador::crear", "AuthMiddleware::authorized");
Routes::Add("/productos/procesarCrear", "post", "ProductoControlador::procesarCrear", "AuthMiddleware::authorized");
Routes::Add("/compras/comprar", "get", "CompraControlador::comprar", "AuthMiddleware::authorized");
Routes::Add("/compras/procesarComprar", "post", "CompraControlador::procesarComprar", "AuthMiddleware::authorized");
Routes::Add("/compras/listar", "get", "CompraControlador::listar", "AuthMiddleware::authorized");

Routes::Run();
